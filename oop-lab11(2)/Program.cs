﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleClassLibrary;

namespace oop_lab11_2_
{
    class Program
    {
        static void InputInfo(int n)
        {
            Console.WriteLine();
            Goods[] products = new Goods[n];
            Goods product = new Goods();

            for(int i = 0; i < n; i++)
            {
                Console.Write("Назва товару: ");
                string NameProduct = Console.ReadLine();
                Console.Write("Ціна товару: ");
                double Price = Convert.ToDouble(Console.ReadLine());
                Console.Write("Вага товару: ");
                double Weight = Convert.ToDouble(Console.ReadLine());

                products[i] = new Goods(NameProduct, Price, Weight);
                Console.WriteLine();
            }
            Console.WriteLine();
            Menu(products, n, product);
        }

        static void OutputInfo(Goods[] student, int n)
        {
            Goods students = new Goods();
            Console.WriteLine("Інформація про товар:");
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine();
                Console.WriteLine($"Назва товару - {student[i].NameProduct}");
                Console.WriteLine($"Ціна товару - {student[i].Price}");
                Console.WriteLine($"Вага товару - {student[i].Weight}");
            }
            Console.WriteLine();
            Menu(student, n, students);
        }

        static void SortByPrice(Goods[] goods, int n)
        {
            Array.Sort(goods, new PriceComparer());
            Console.WriteLine("Відсортовано за ціною:");
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine();
                Console.WriteLine($"Назва товару - {goods[i].NameProduct}");
                Console.WriteLine($"Ціна товару - {goods[i].Price}");
                Console.WriteLine($"Вага товару - {goods[i].Weight}");
            }
        }

        static void SortByPriceAndWeight(Goods[] goods, int n)
        {
            Array.Sort(goods, new PriceAndWeightComparer());
            Console.WriteLine("Відсортовано за ціною та вагою:");
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine();
                Console.WriteLine($"Назва товару - {goods[i].NameProduct}");
                Console.WriteLine($"Ціна товару - {goods[i].Price}");
                Console.WriteLine($"Вага товару - {goods[i].Weight}");
            }

            
        }

        static void Menu(Goods[] _goods, int n, Goods goods)
        {
            do
            {
                Console.WriteLine("Меню:");
                Console.WriteLine("1. Записати інформацію");
                Console.WriteLine("2. Сортування за ціною");
                Console.WriteLine("3. Сортування за ціною та вагою");
                Console.WriteLine("4. Вивести інформацію");
                Console.WriteLine("5. Вийти");
                Console.WriteLine();

                int k;
                do
                {
                    Console.Write("Виберіть дію: ");
                    if (!int.TryParse(Console.ReadLine(), out k))
                        Console.WriteLine("Помилка введеня значення! Будь ласка повторіть введене значення ще раз!");
                    else
                        break;
                } while (true);

                Console.WriteLine();

                Goods[] products = _goods;
                Goods product = new Goods();
                switch (k)
                {
                    case 1:
                        InputInfo(n);
                        break;
                    case 2:
                        SortByPrice(_goods, n);
                        break;
                    case 3:
                        SortByPriceAndWeight(_goods, n);
                        break;
                    case 4:
                        OutputInfo(_goods, n);
                        break;
                    case 5:
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Помилка!");
                        break;
                }
                Console.WriteLine();
            } while (true);

        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            int n;
            do
            {
                Console.Write("Введіть кількість товару: ");
                if (!int.TryParse(Console.ReadLine(), out n))
                    Console.WriteLine("Помилка введеня значення! Будь ласка повторіть введене значення ще раз!");
                else
                    break;
            } while (true);
            Console.WriteLine();

            Goods[] products = new Goods[n];
            Goods product = new Goods();

            Menu(products, n, product);

            Console.ReadKey();
        }
    }
}
