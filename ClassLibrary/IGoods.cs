﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public interface IGoods
    {
        string NameProduct { get; set; }

        double Weight { get; set; }

        void InputInfo();

        void PrintInfo();
    }
}
