﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
   public class Toy : Goods
    {
        public override string NameProduct { get; set; }

        public override double Weight { get; set; }

        public override void InputInfo()
        {

        }

        public override void PrintInfo()
        {

        }
    }
}
