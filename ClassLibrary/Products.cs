﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Products : Goods
    {
        private string nameProduct;

        public override string NameProduct
        {
            get { return nameProduct; }
            set { nameProduct = value; }
        }

        private double weight;

        public override double Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        public Products()
        {

        }

        public Products(string nameProduct, double weight)
        {
            this.nameProduct = nameProduct;
            this.weight = weight;
        }

        public Products(Products products)
        {
            this.NameProduct = products.NameProduct;
            this.Weight = products.Weight;
        }

        public override void InputInfo()
        {
            Console.Write("Назва товару: ");
            NameProduct = Console.ReadLine();
            Console.Write("Вага товару: ");
            Weight = Convert.ToDouble(Console.ReadLine());
        }

        public override void PrintInfo()
        {
            Console.WriteLine("Інформація про товар:");
            Console.WriteLine($"1. Назва товару - {NameProduct}");
            Console.WriteLine($"2. Вага товару - {Weight}");
        }
    }
}
