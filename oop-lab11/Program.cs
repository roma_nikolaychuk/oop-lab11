﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary;

namespace oop_lab11
{
    class Program
    {
        static void Output(IGoods products)
        {
            products.PrintInfo();
        }

        static void Input(IGoods products)
        {
            products.InputInfo();
        }

        static void Menu()
        {
            Products products = new Products();
            do
            {
                Console.WriteLine("Меню:");
                Console.WriteLine("1. Записати інформацію");
                Console.WriteLine("2. Вивести інформацію");
                Console.WriteLine("3. Вийти");
                Console.WriteLine();
                
                int k;
                do
                {
                    Console.Write("Виберіть дію: ");
                    if (!int.TryParse(Console.ReadLine(), out k))
                        Console.WriteLine("Помилка введеня значення! Будь ласка повторіть введене значення ще раз!");
                    else
                        break;
                } while (true);

                Console.WriteLine();

                switch (k)
                {
                    case 1:
                        Input(products);
                        break;
                    case 2:
                        Output(products);
                        break;
                    case 3:
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Помилка!");
                        break;
                }
                Console.WriteLine();
            } while (true);

        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Menu();
      
            Console.ReadKey();
        }
    }
}
