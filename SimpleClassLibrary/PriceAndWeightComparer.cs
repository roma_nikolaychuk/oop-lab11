﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{
    public class PriceAndWeightComparer : IComparer<Goods>
    {
        public int Compare(Goods x, Goods y)
        {
            Goods g1 = x as Goods;
            Goods g2 = y as Goods;

            if (g1 == null || g2 == null)
                throw new ArgumentException("Один або більше аргументів не сумісні із класом Square.");
            if (g1.Price > g2.Price || g1.Weight > g2.Weight) return 1;
            if (g1.Price < g2.Price || g1.Weight < g2.Weight) return -1;
            return 0;
        }
    }
}
