﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{
    public class PriceComparer : IComparer<Goods>
    {
        public int Compare(Goods x, Goods y)
        {
            if (x.Price < y.Price)
                return -1;
            else if (x.Price > y.Price)
                return 1;
            else
                return 0;
        }
    }
}
