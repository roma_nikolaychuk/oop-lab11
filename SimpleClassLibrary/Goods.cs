﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{
    public class Goods : IComparable
    {
        public int CompareTo(object obj)
        {
            Goods g = obj as Goods;
            if (g != null)
            {
                if (this.Price < g.Price)
                    return -1;
                else if (this.Price > g.Price)
                    return 1;
                else
                    return 0;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                throw new Exception("Неможливо порівняти два об'єкта");
            }
        }


        private string nameProduct;

        public string NameProduct
        {
            get { return nameProduct; }
            set { nameProduct = value; }
        }

        private double price;

        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        private double weight;

        public double Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        public Goods()
        {

        }

        public Goods(string nameProduct, double price, double weight)
        {
            this.nameProduct = nameProduct;
            this.price = price;
            this.weight = weight;
        }

        public Goods(Goods goods)
        {
            this.NameProduct = goods.NameProduct;
            this.Price = goods.Price;
            this.Weight = goods.Weight;
        }
    }
}
